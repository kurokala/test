﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxScript : MonoBehaviour {

    Animator anim;
    TimerShowOnUI testbug;

    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        testbug = GameObject.Find("GameManager").GetComponent<TimerShowOnUI>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform == transform)
                {
                    anim.SetTrigger("onClickBox");
                    testbug.doit() ;
                    Destroy(gameObject, 1);
                }
            }
        }
    }
}
