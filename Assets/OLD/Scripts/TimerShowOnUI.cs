﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimerShowOnUI : MonoBehaviour
{
    public Text timerLabel;
    public Text hScore;
    public Text winshow;
    public Text boxesLabel;
    bool boolThing;
    static int enemiesLeft;
    float highscoreval;
    string highscore;
    static GameObject[] respawns;
    int boxesHit;
    private float time;
    void Start()
    {
        boolThing = false;
        if (!PlayerPrefs.HasKey("highscoreval"))
        {
            PlayerPrefs.SetFloat("highscoreval", 59);
            PlayerPrefs.SetString("highscore", "00 : 59 : 999");
        }
        highscore = PlayerPrefs.GetString("highscore", highscore);
        highscoreval = PlayerPrefs.GetFloat("highscoreval", highscoreval);
        hScore.text = "Lowest time: "+highscore;
        boxesHit = 0;
    }
    public static void beginOperation()
    {
        //GameObject[] respawns;
        if (respawns == null)
            respawns = GameObject.FindGameObjectsWithTag("Respawn");

        foreach (GameObject respawn in respawns)
        {
            enemiesLeft += 1;
        }
        Debug.Log("Boxes generated = " + enemiesLeft);
    }
    public void doit()
    {
        boxesHit += 1;
    }
    void Update()
    {
        time += Time.deltaTime;

        var minutes = time / 60; //Divide the guiTime by sixty to get the minutes.
        var seconds = time % 60;//Use the euclidean division for the seconds.
        var fraction = (time * 100) % 100;

        //update the label value
        timerLabel.text = string.Format("{0:00} : {1:00} : {2:000}", minutes, seconds, fraction);
        boxesLabel.text = ("Boxes left: "+(enemiesLeft-boxesHit));
        if((enemiesLeft - boxesHit)<1 && boolThing==false)
        {
            boolThing = true;
            //winshow.fontSize=0;
            if (time > highscoreval)
            {
                Debug.Log("Your score is "+timerLabel.text+", highscore unchanged is "+highscore);
                winshow.text = "Your score = "+timerLabel.text +" \nUnbeaten Lowest Time: " + highscore;
            }
            else
            {
                highscoreval = time;
                highscore = timerLabel.text ;
                PlayerPrefs.SetString("highscore", highscore);
                PlayerPrefs.SetFloat("highscoreval", highscoreval);
                Debug.Log("Your score is " + timerLabel.text + ", new highscore is " + highscore);
                winshow.text = "New Lowest Time: " + highscore;
            }
        }
    }
}