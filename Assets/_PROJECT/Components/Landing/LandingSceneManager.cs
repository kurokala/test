﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LandingSceneManager : MonoBehaviour {

    #region Refs

    [Header("Landing")]
    public GameObject pnlLanding;
    //public Button btnStart;

    [Header("Loading")]
    public GameObject pnlLoading;

    [Header("Login")]
    public GameObject pnlLogin;

    [Header("Dashboard")]
    public GameObject pnlDashboard;

    [Header("Other")]
    public GameObject NonUI;
    public GameObject frame;
    #endregion

    // Use this for initialization
    void Start () {
        LoadPanel(Panels.Landing);
    }
	
    #region UI Service
    public void KillEverything()
    {
        ResetPanels();
        Application.Quit();
    }
    public void GotoLoading()
    {
        LoadPanel(Panels.Loading);
        GotoDashboard(); //WhatamIdoing?????
    }

    public void GotoLanding()
    {
        LoadPanel(Panels.Landing);
    }
    public void GotoLogin()
    {
        LoadPanel(Panels.Login);
    }
    public void GotoDashboard()
    {
        Wait(2, () =>
        {
            LoadPanel(Panels.Dashboard);
            frame.SetActive(false);
            NonUI.SetActive(true);
        });
    }
    #endregion

    #region Panel Management
    void ResetPanels()
    {
        pnlLoading.SetActive(false);
        pnlLanding.SetActive(false);
        pnlLogin.SetActive(false);
        pnlDashboard.SetActive(false);
        NonUI.SetActive(false);
        frame.SetActive(true);
    }

    public void LoadPanel(Panels panel)
    {
        ResetPanels();
        switch (panel)
        {
            case Panels.Landing:
                pnlLanding.SetActive(true);
                break;

            case Panels.Loading:
                pnlLoading.SetActive(true);
                break;
            case Panels.Login:
                pnlLogin.SetActive(true);
                break;
            case Panels.Dashboard:
                pnlDashboard.SetActive(true);
                break;
        }
    }

    #endregion

    #region Utilities
    IEnumerator wait(float sec, System.Action Callback)
    {
        yield return new WaitForSeconds(sec);
        Callback();
    }

    void Wait(float sec, System.Action Callback)
    {
        StartCoroutine(wait(sec, Callback));
    }
    #endregion

    public enum Panels
    {
        Landing,
        Loading,
        Login,
        Dashboard
    }
}
