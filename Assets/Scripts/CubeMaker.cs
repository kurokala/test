﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CubeMaker : MonoBehaviour {
    CubeClass[] listOfData;
    GameObject alpha;
    public GameObject test;
    string json;
    [System.Serializable]
    public class CubeClass
    {
        public string id;
        public string username;
    }

    IEnumerator Start()
    {
        string url = "https://jsonplaceholder.typicode.com/users";
        WWW www = new WWW(url);
        yield return www;
        if (www.error == null)
        {
            json = www.text;
            //Debug.Log(json);        //the string has the database
            ReadData();
            Debug.Log("active");
        }
        else
        {
            Debug.Log("ERROR: " + www.error);       //if string or database fails to load
        }
        //Invoke("BeginOperation",1);
        BeginOperation();
    }

    private void ReadData()
    {
        //to convert online database data in string json into array 
        listOfData = JsonHelper.getJsonArray<CubeClass>(json);
    }
    void BeginOperation()
    {
        for (int zz = 0; zz < 10;zz++)
        {
            alpha=Instantiate(test, new Vector3(Random.Range(-11,11),1, Random.Range(-11, 11)), Quaternion.identity);

            TextMesh[] t = alpha.GetComponentsInChildren<TextMesh>();
            t[0].text = listOfData[zz].username;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
