﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LandingButtons : MonoBehaviour {
    public Button start,close,settings;
    public GameObject nextScreen;
	// Use this for initialization
	void Start () {
        start.onClick.AddListener(StartFunc);
        settings.onClick.AddListener(SettingsFunc);
        close.onClick.AddListener(CloseFunc);
    }
    void StartFunc()
    {
        Debug.Log("Load next UI, close this UI");
        nextScreen.SetActive(true);
        Debug.Log("This works fine");
        GameObject.Find("Landing Page").SetActive(false);
    }
	void SettingsFunc()
    {
        Debug.Log("Editing settings");
    }
    void CloseFunc()
    {
        Debug.Log("Clicked close");
        GameObject.Find("Landing Page").SetActive(false);
        Application.Quit();
    }
}
