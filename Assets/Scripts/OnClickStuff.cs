﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnClickStuff : MonoBehaviour
{
    // Use this for initialization
    public ObjectPool ObjPool;
    bool clicked = false;
    public Image xxx;
    public Image displayImage;
    public Button buttonComponent;
    public Text idText;

    void Start()
    {
        buttonComponent.onClick.AddListener(selectorFunc);
        ObjPool = GameObject.Find("ObjectPoolHolder").GetComponent<ObjectPool>();
    }

    public void selectorFunc()
    {
        if (clicked == false)
        {
            ObjPool.pushIntoList(gameObject);
            clicked = true;
            xxx.GetComponent<Image>().color = new Color32(100, 255, 225, 100);
        }
        else
        {
            ObjPool.removeFromList(gameObject);
            clicked = false;
            xxx.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
    }
}
