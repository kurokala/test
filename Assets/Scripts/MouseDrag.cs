﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseDrag : MonoBehaviour {
    Color testColor;
    private Vector3 screenPoint;
    Rigidbody m_Rigidbody;
    readonly float movementSpeed = 8f;
    //readonly float torque = 5f;
    bool selected;
    bool rotationSelected;
    private Vector3 offset;
    void Start()
    {
        //testColor = new Color(0, 255, 215, 255); //TRHOWS THE BIGGEST ERROR OF 2018
        testColor = Color.cyan;
        m_Rigidbody = GetComponent<Rigidbody>();
        selected = false;
        rotationSelected = false;
        m_Rigidbody.constraints = RigidbodyConstraints.FreezeAll;
    }
    void Update()
    {
        //other controls, is there more efficient way to detect input?
        if (Input.GetKeyDown(KeyCode.R)) if (selected) executeEquality();
        if (Input.GetKeyDown(KeyCode.Delete)) if (selected) Destroy(gameObject);
        if (Input.GetKeyDown(KeyCode.LeftShift)) if (selected) {rotationSelected = true; gameObject.GetComponent<Renderer>().material.color = Color.green; }
        if (Input.GetKeyDown(KeyCode.LeftAlt)) if (selected) {rotationSelected = false; gameObject.GetComponent<Renderer>().material.color = Color.red; }
            //raycast detection for object selection
        if (Input.GetMouseButtonDown(0))
        {
            Ray castPoint = Camera.main.ScreenPointToRay(Input.mousePosition);//Cast a ray to get where the mouse is pointing at
            RaycastHit hit;//Stores the position where the ray hit.
            if (Physics.Raycast(castPoint, out hit, Mathf.Infinity))//If the raycast doesnt hit a wall
            {
                if (hit.transform == transform)
                {
                    m_Rigidbody.constraints = RigidbodyConstraints.None;
                    if(rotationSelected)
                    {
                        gameObject.GetComponent<Renderer>().material.color = Color.green;
                        m_Rigidbody.constraints = RigidbodyConstraints.FreezePosition;
                    }
                    else
                    {
                        gameObject.GetComponent<Renderer>().material.color = Color.red;
                        m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
                    }
                    selected = true;
                    Debug.Log("True: " + transform);
                }
                else
                {
                    selected = false;
                    gameObject.GetComponent<Renderer>().material.color = testColor;
                    m_Rigidbody.constraints = RigidbodyConstraints.FreezeAll;
                }
            }
        }
    }
    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position); //without this objects are jumping up for some reason, fix this
    }

    void OnMouseDrag()
    {
        //having trouble with rotating
        //float turn = Input.GetAxis("Horizontal");
        //m_Rigidbody.AddTorque(Vector3.forward * torque * turn);
        //transform.Rotate(0, (Input.GetAxis("Mouse X") * torque * -Time.deltaTime), 0, Space.World);

        Vector3 moveTowards = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z))-transform.position;
        m_Rigidbody.velocity = (moveTowards*movementSpeed);
        m_Rigidbody.AddTorque(new Vector3(0, 90, 0));
    }
    //alternate, makes it look unsmooth
    void OnMouseUp()
    {
        m_Rigidbody.velocity = Vector3.zero;
        m_Rigidbody.angularVelocity = Vector3.zero;
    }
    void executeEquality()
    {
        //rotation back to default position
        transform.rotation = Quaternion.identity;
    }
}
