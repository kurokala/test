﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserTracker : MonoBehaviour {
    Color testColor;
    private Vector3 screenPoint;
    Rigidbody m_Rigidbody;
    readonly float movementSpeed = 8f;
    readonly float torque = 5f;
    bool selected;
    bool rotationSelected;
    private Vector3 offset;

    // Use this for initialization
    void Start () {
        testColor = Color.cyan;
        m_Rigidbody = GetComponent<Rigidbody>();
        selected = false;
        rotationSelected = false;
        m_Rigidbody.constraints = RigidbodyConstraints.FreezeAll;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
