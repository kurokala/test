﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Instantiator : MonoBehaviour {
    public GameObject test;
    public Transform contentPanel;
    public ObjectPool ObjPool;
    //GameObject spawnedGameObject;
    public Button adder, destroyer;
    public Image displayImage;
    ImageButtonClass[] listOfData;
    int itemCount;
    //Aslo change displayimg to selectedimg

    //new
    GameObject alpha;
    string json;
    [System.Serializable]
    public class ImageButtonClass
    {
        public string albumId;
        public string id;
        public string title;
        public string url;
        public string thumbnailUrl;
    }

    IEnumerator Start()
    {
        itemCount = 0;
        adder.onClick.AddListener(addStuff);
        destroyer.onClick.AddListener(destroyStuff);
        adder.interactable = false;
        ObjPool = GameObject.Find("ObjectPoolHolder").GetComponent<ObjectPool>();

        string url = "https://jsonplaceholder.typicode.com/photos";
        WWW www = new WWW(url);
        yield return www;
        if (www.error == null)
        {
            json = www.text;
            //Debug.Log(json);        //the string has the database
            ReadData();
            adder.interactable = true;
            Debug.Log("active");
            adder.GetComponentInChildren<Text>().text = "Add new items";
        }
        else
        {
            Debug.Log("ERROR: " + www.error);       //if string or database fails to load
        }
    }


    private void ReadData()
    {
        //to convert online database data in string json into array 
        listOfData = JsonHelper.getJsonArray<ImageButtonClass>(json);
        //test that its working:
        //foreach (ImageButtonClass a in listOfData)
        //{
        //    Debug.Log(a.id);
        //}
    }

    void addStuff()
    {
        adder.interactable = false;
        adder.GetComponentInChildren<Text>().text = "Loading";
        StartCoroutine("adderCoroutine");
    }
    IEnumerator adderCoroutine()
    {
        int x = Random.Range(10, 16);
        Debug.Log("Adding " + x + " Objects");
        for (int i = 0; i < x; i++)
        {
            WWW www1 = new WWW(listOfData[itemCount].thumbnailUrl);
            yield return www1;
            alpha = GameObject.Instantiate(test, contentPanel);
            Text[] t = alpha.GetComponentsInChildren<Text>();
            Image[] im = alpha.GetComponentsInChildren<Image>();
            //t[2].text = "b";
            im[1].sprite = Sprite.Create(www1.texture, new Rect(0, 0, www1.texture.width, www1.texture.height), new Vector2(0, 0));
            t[1].text = listOfData[itemCount].id;
            t[0].text = listOfData[itemCount].title;
            itemCount = (itemCount + 1) % 5000; //still throws error when exceeding 5k
        }
        adder.interactable = true;
        adder.GetComponentInChildren<Text>().text = "Add new items";
    }

    void destroyStuff()
    {
        ObjPool.desutroyModo();
        displayImage.sprite = null;
    }
}

//new helper class
public class JsonHelper
{
    public static T[] getJsonArray<T>(string json)
    {
        string newJson = "{ \"array\": " + json + "}";
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
        return wrapper.array;
    }

    [System.Serializable]
    private class Wrapper<T>
    {
        public T[] array;
    }
}