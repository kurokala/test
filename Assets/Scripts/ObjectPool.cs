﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectPool:MonoBehaviour{
    public GameObject[] thisList;
    int count;
    void Start()
    {
        count = 0;
        thisList= new GameObject[100];
    }
    // Use this for initialization
    public void pushIntoList(GameObject gObject)
    {
        Debug.Log("" + count);
        thisList[count]=gObject;
        count++;
    }
    public void removeFromList(GameObject gObject)
    {
        for(int i=0;i<count;i++)
        {
            if(thisList[i]==gObject)
            {
                breakList(i);
                break;
            }
        }
    }
    void breakList(int i)
    {
        for(;i<count;i++)
        {
            thisList[i] = thisList[i + 1];
        }
        count--;
    }
    public void desutroyModo()
    {
        for(int i=0;i<count;i++)
        {
            Destroy(thisList[i]);
        }
        Debug.Log("Its all over now");
        count = 0;
    }
}
